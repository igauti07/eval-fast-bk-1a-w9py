/****** Object:  Database [brighthr_dev]  ******/
IF NOT EXISTS (
        SELECT *
        FROM sys.databases
        WHERE name = 'FortCodeDb'
        )
BEGIN
    CREATE DATABASE [FortCodeDb]
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='UserAccounts' and xtype='U')
    USE [FortCodeDb]
    GO
    CREATE TABLE UserAccounts (
        Id INT IDENTITY NOT NULL CONSTRAINT PK_UserAccounts PRIMARY KEY,
        Name varchar(100) not null,
        Email varchar(100) not null,
        Password varchar(100) not null,
    )
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='FavoriteCity' and xtype='U')
    USE [FortCodeDb]
    GO
    CREATE TABLE FavoriteCity (
        Id INT IDENTITY NOT NULL CONSTRAINT PK_FavoriteCity PRIMARY KEY,
        City varchar(100) not null,
        Country varchar(100) not null,
        UserId INT not null,
    )
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='FavoriteBar' and xtype='U')
    USE [FortCodeDb]
    GO
    CREATE TABLE FavoriteBar (
        Id INT IDENTITY NOT NULL CONSTRAINT PK_FavoriteBar PRIMARY KEY,
        BarName varchar(100) not null,
        FavoriteDrink varchar(100) not null,
        CityId INT not null,
    )
GO