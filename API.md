#### API Usage
Note: Calls to this api require a basic auth header set.
##### Create User (POST)
/api/user

```javascript
Example Request:   
{
   "Name" : "goutham",
   "Email": "igauti@gmail.com",
   "Password":"test" 
}
```
##### Get User (GET)
/api/user/1

```javascript
Example Response:   
{
   "Name" : "goutham",
   "Email": "igauti@gmail.com",
   "Password":"test" 
}
```

##### Get All Users (GET)
/api/user/

```javascript
Example Response:   
[{
   "Name" : "goutham",
   "Email": "igauti@gmail.com",
   "Password":"test" 
}]
```
##### Create FavoriteCity (POST)
/api/favoriteCities

```javascript
Example Request:   
{
    "City" : "OHK",
    "Country": "USA"
}
```
##### Get FavoriteCities (GET)
/api/favoriteCities

```javascript
Example Response:   
[
    {
        "id": 1,
        "city": "OHK",
        "country": "USA"
    },
    {
        "id": 2,
        "city": "MICHIGAN",
        "country": "USA"
    }
]
```
##### Create FavoriteBar (POST)
/api/favoriteCities/1/bars

```javascript
Example Request:   
{
    "BarName" : "AllOne",
    "FavoriteDrink": "Tequila"
}
```
##### Get All FavoriteBars (GET)
/api/favoriteCities/1/bars

```javascript
Example Response:   
[{
    "Id": 1,
    "BarName" : "AllOne",
}[]
```
##### Get All FavoriteDrinks (GET)
/api/favoriteCities/1/drinks

```javascript
Example Response:   
[{
    "Id": 1,
    "BarName" : "AllOne",
    "FavoriteDrink": "Tequila"
}[]
```