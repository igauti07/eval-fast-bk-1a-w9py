﻿namespace FortCode
{
    public class UserAccountApiModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}