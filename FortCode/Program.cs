﻿using System.Data.Common;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace FortCode
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(((context, config) =>
                {
                    config.AddEnvironmentVariables("FORTCODEENV_");
                    config.AddJsonFile("appsettings.json");
                }))
                .UseStartup<Startup>();
    }
}