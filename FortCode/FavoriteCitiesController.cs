﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FortCode
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FavoriteCitiesController : ControllerBase
    {
        private readonly IFavoriteCityRepository _favoriteCityRepository;
        private readonly IUserService _userService;

        public FavoriteCitiesController(IFavoriteCityRepository favoriteCityRepository, IUserService userService)
        {
            _favoriteCityRepository = favoriteCityRepository;
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateFavoriteCity([FromBody] CityApiModel request)
        {
            return new OkObjectResult(await _favoriteCityRepository.AddFavoriteCity(request.City, request.Country, int.Parse(User.Claims.First().Value)));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllFavoriteCities()
        {
            return new OkObjectResult(await _favoriteCityRepository.GetFavoriteCities( int.Parse(User.Claims.First().Value)));
        }

        [HttpPost("{cityId}/bars")]
        public async Task<IActionResult> AddSecretBar([FromRoute]int cityId, [FromBody] BarsApiModel favoriteBarRequest)
        {
            return new OkObjectResult(await _favoriteCityRepository.AddFavoriteBar(favoriteBarRequest, cityId));
        }

        [HttpGet("{cityId}/bars")]
        public async Task<IActionResult> GetSecretBars(int cityId)
        {
            return new OkObjectResult(await _favoriteCityRepository.GetAllFavoriteBars(cityId));
        }

        [HttpGet("{cityId}/drinks")]
        public async Task<IActionResult> GetFavoriteDrinks(int cityId)
        {
            return new OkObjectResult(await _favoriteCityRepository.GetAllFavoriteDrinks(cityId));
        }
    }
}
