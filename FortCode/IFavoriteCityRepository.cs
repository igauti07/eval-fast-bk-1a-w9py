using System.Collections.Generic;
using System.Threading.Tasks;

namespace FortCode
{
    public interface IFavoriteCityRepository
    {
        Task<int> AddFavoriteCity(string cityName, string country, int userId);
        Task<IEnumerable<FavoriteCity>> GetFavoriteCities(int userId);
        Task<int> AddFavoriteBar(BarsApiModel favoriteBarRequest, int cityId);
        Task<IEnumerable<FavoriteBarsDto>> GetAllFavoriteBars(int cityId);
        Task<IEnumerable<FavoriteDrinkDto>> GetAllFavoriteDrinks(int cityId);
    }
}