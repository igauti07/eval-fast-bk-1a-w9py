﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FortCode
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserAccountRepository _userAccountRepository;

        public UserController(IUserAccountRepository userAccountRepository)
        {
            _userAccountRepository = userAccountRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] UserAccountApiModel user)
        {
            var result = await _userAccountRepository.CreateUserAccount(user.Name, user.Email, user.Password);
            return Ok(new {Id = result});
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            var result = await _userAccountRepository.GetAllUserAccounts();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            var result = await _userAccountRepository.GetUserAccount(id);
            return Ok(result);
        }
    }
}
