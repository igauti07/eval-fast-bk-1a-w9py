using System.Data;

namespace FortCode
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateDbConnection();
    }
}