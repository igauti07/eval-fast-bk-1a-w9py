using System.Collections.Generic;
using System.Threading.Tasks;

namespace FortCode
{
    public class FavoriteCityRepository : IFavoriteCityRepository
    {
        private readonly IQuery _query;

        public FavoriteCityRepository(IQuery query)
        {
            _query = query;
        }

        public async Task<int> AddFavoriteCity(string cityName, string country, int userId)
        {
            const string sql = @"
                   INSERT INTO dbo.FavoriteCity
                   (City, Country, userId)
                   Values
                   (@City, @Country, @UserId);
                   SELECT CAST(SCOPE_IDENTITY() as BIGINT)
                ";

            return await _query.QueryFirstAsync<int>(sql, new
            {
                City = cityName,
                Country = country,
                UserId = userId
            });
        }

        public async Task<IEnumerable<FavoriteCity>> GetFavoriteCities(int userId)
        {
            const string sql = @" Select Id,City,Country From dbo.FavoriteCity Where UserId=@UserId";

            return await _query.QueryAsync<FavoriteCity>(sql, new
            {
                UserId = userId
            });
        }

        public async Task<int> AddFavoriteBar(BarsApiModel favoriteBarRequest, int cityId)
        {
            const string sql = @"
                   INSERT INTO dbo.FavoriteBar
                   (BarName, FavoriteDrink, CityId)
                   Values
                   (@Bar, @Drink, @CityId);
                   SELECT CAST(SCOPE_IDENTITY() as BIGINT)
                ";

            return await _query.QueryFirstAsync<int>(sql, new
            {
                Bar = favoriteBarRequest.BarName,
                Drink = favoriteBarRequest.FavoriteDrink,
                CityId = cityId
            });
        }

        public async Task<IEnumerable<FavoriteBarsDto>> GetAllFavoriteBars(int cityId)
        {
            const string sql = @" Select Id,BarName From dbo.FavoriteBar Where CityId=@CityId";

            return await _query.QueryAsync<FavoriteBarsDto>(sql, new
            {
                CityId = cityId
            });
        }

        public async Task<IEnumerable<FavoriteDrinkDto>> GetAllFavoriteDrinks(int cityId)
        {
            const string sql = @" Select Id,BarName,FavoriteDrink From dbo.FavoriteBar Where CityId=@CityId";

            return await _query.QueryAsync<FavoriteDrinkDto>(sql, new
            {
                CityId = cityId
            });
        }
    }
}