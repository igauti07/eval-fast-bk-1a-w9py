﻿namespace FortCode
{
    public class BarsApiModel
    {
        public string BarName { get; set; }
        public string FavoriteDrink { get; set; }
    }
}