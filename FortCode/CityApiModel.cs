﻿namespace FortCode
{
    public class CityApiModel
    {
        public string City { get; set; }
        public string Country { get; set; }
    }
}