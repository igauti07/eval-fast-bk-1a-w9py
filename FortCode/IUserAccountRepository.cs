using System.Collections.Generic;
using System.Threading.Tasks;

namespace FortCode
{
    public interface IUserAccountRepository
    {
        Task<int> CreateUserAccount(string name, string email, string password);
        Task<UserAccount> GetUserAccount(int id);
        Task<IEnumerable<UserAccount>> GetAllUserAccounts();
        Task<UserAccount> GetUserAccountByEmailAndPassword(string email,string password);
    }
}