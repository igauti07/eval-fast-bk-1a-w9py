using System.Collections.Generic;
using System.Threading.Tasks;

namespace FortCode
{
    public interface IQuery
    {
        Task<IEnumerable<T>> QueryAsync<T>(string sql);

        Task<IEnumerable<T>> QueryAsync<T>(string sql, object parameters);

        Task<T> QueryFirstAsync<T>(string sql);

        Task<T> QueryFirstAsync<T>(string sql, object parameters);

        Task<T> QueryFirstOrDefaultAsync<T>(string sql, object parameters);

        Task<int> ExecuteAsync(string sql);

        Task<int> ExecuteAsync(string sql, object parameters);

    }
}