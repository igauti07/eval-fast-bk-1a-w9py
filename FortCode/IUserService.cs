using System.Threading.Tasks;

namespace FortCode
{
    public interface IUserService
    {
        Task<UserAccount> Authenticate(string email, string password);
    }
}