namespace FortCode
{
    public class DbContextDapperQuery : DapperQuery
    {
        public DbContextDapperQuery(IDbConnectionFactory factory) : base(factory.CreateDbConnection())
        {
        }
    }
}