using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;

namespace FortCode
{
    public abstract class DapperQuery : IQuery
    {
        private readonly IDbConnection _connection;

        protected DapperQuery(IDbConnection connection)
        {
            _connection = connection;
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string sql)
        {
            return await _connection.QueryAsync<T>(sql);
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string sql, object parameters)
        {
            return await _connection.QueryAsync<T>(sql, parameters);
        }

        public async Task<T> QueryFirstAsync<T>(string sql)
        {
            return await _connection.QueryFirstAsync<T>(sql);
        }

        public async Task<T> QueryFirstAsync<T>(string sql, object parameters)
        {
            return await _connection.QueryFirstAsync<T>(sql, parameters);
        }
        public async Task<T> QueryFirstOrDefaultAsync<T>(string sql, object parameters)
        {
            return await _connection.QueryFirstOrDefaultAsync<T>(sql, parameters);
        }

        public async Task<int> ExecuteAsync(string sql)
        {
            return await _connection.ExecuteAsync(sql);
        }

        public async Task<int> ExecuteAsync(string sql, object parameters)
        {
            return await _connection.ExecuteAsync(sql, parameters);
        }
    }
}