namespace FortCode
{
    public class FavoriteDrinkDto
    {
        public int Id { get; set; }
        public string BarName { get; set; }
        public string FavoriteDrink { get; set; }
    }
}