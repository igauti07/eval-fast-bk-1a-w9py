namespace FortCode
{
    public class FavoriteBarsDto
    {
        public int Id { get; set; }
        public string BarName { get; set; }
    }
}