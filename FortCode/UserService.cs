using System.Threading.Tasks;

namespace FortCode
{
    public class UserService : IUserService
    {
        // users injected via config for simplicity, store in a db with hashed passwords in production applications
        private readonly IUserAccountRepository _userAccountRepository;

        public UserService(IUserAccountRepository userAccountRepository)
        {
            _userAccountRepository = userAccountRepository;
        }

        public async Task<UserAccount> Authenticate(string email, string password)
        {
            var user = await _userAccountRepository.GetUserAccountByEmailAndPassword(email, password);

            // return null if user not found
            if (null == user)
                return null;

            // authentication successful so return user details without password
            user.Password = null;
            return user;
        }
    }
}