using System.Collections.Generic;
using System.Threading.Tasks;

namespace FortCode
{
    public class UserAccountRepository : IUserAccountRepository
    {
        private readonly IQuery _query;

        public UserAccountRepository(IQuery query)
        {
            _query = query;
        }

        public async Task<int> CreateUserAccount(string name, string email, string password)
        {
            const string sql = @"
                   INSERT INTO dbo.UserAccounts
                   (Name, Email, Password)
                   Values
                   (@Name, @Email, @Password);
                   SELECT CAST(SCOPE_IDENTITY() as BIGINT)
                ";

            return await _query.QueryFirstAsync<int>(sql, new
            {
                Name = name,
                Email = email,
                Password = password
            });
        }

        public async Task<UserAccount> GetUserAccount(int id)
        {
            const string sql = @"
                   Select Id,Name,Email,Password From dbo.UserAccounts Where Id=@Id";

            return await _query.QueryFirstAsync<UserAccount>(sql, new
            {
                Id = id
            });
        }

        public async Task<IEnumerable<UserAccount>> GetAllUserAccounts()
        {
            const string sql = @"
                   Select Id,Name,Email,Password From dbo.UserAccounts";

            return await _query.QueryAsync<UserAccount>(sql);
        }

        public async Task<UserAccount> GetUserAccountByEmailAndPassword(string email,string password)
        {
            const string sql = @"
                   Select Id,Name,Email,Password From dbo.UserAccounts Where email=@Email AND password=@Password";

            return await _query.QueryFirstOrDefaultAsync<UserAccount>(sql, new
            {
                Email = email,
                Password = password
            });
        }
    }
}