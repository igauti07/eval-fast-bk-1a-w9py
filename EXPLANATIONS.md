There is automated test coverage for the application. 
There is postman collection attached at the root of the repo and test can be run from either command line interface using tool called `newman` cli or import the collection into postman client.
The Url may need adjusting if you running the host inside docker.

The app uses Basic Auth to auth api requests.
The Database tables are provisioned using sql scripts that run part of the docker bootstrap using shell scripts. 
I tried using CodeFirst approch using Entity Framework and it felt bit too much for the code exercise given up and gone down this route.

The App uses Dapper ORM to map SQL data into c# objects.

The task to share the bars between users is not implemented and it can be implemented by having a link table and additional endpoints to share a bar with users. Given some more time allocated dont think implementing it would be a problem.

### Instructions to run the application

docker-compose build && docker-compose up

### To run automated tests
npx newman run PostmanCollection.postman_Collection.json
